from django.shortcuts import render
from django.urls import reverse_lazy
from django.views import generic
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
import logging
from .models import Memo
from .forms import InquiryForm , MemoCreateForm

logger= logging.getLogger(__name__)

class IndexView(generic.TemplateView):
    template_name = "index.html"

class InquiryView(generic.TemplateView):
    template_name = "inquiry.html"
    form_class = InquiryForm
    success_url = reverse_lazy('memo:inquiry')

    def form_valid(self,form):
        form.send_email()
        logger.info('Inquiry sent by {}'.format(form.cleaned_data['name']))
        return super().form_valid(form)



class MemoListView(LoginRequiredMixin,generic.ListView):
    model = Memo
    template_name =  "memo_list.html"
    pageinate_by = 2

    def get_queryset(self):
        memos = Memo.objects.filter(user=self.request.user).order_by('created_at')
        return memos

class MemoDetailView(LoginRequiredMixin,generic.DetailView):
    model = Memo
    template_name = 'memo_detail.html'
    #pk_url_kwarg = 'id'

class MemoCreateView(LoginRequiredMixin,generic.CreateView):
    model = Memo
    template_name = 'memo_create.html'
    form_class = MemoCreateForm
    success_url = reverse_lazy('memo:memo_list')

    def form_valid(self, form):
        memo = form.save(commit=False)
        memo.user = self.request.user
        memo.save()
        messages.success(self.request,"メモを作成しました。")
        return super().form_valid(form)
    def form_invalid(self, form):
        messages.error(self.request,"メモの作成に失敗しました")

class MemoUpdateView(LoginRequiredMixin,generic.UpdateView):
    model = Memo
    template_name = 'memo_update.html'
    form_class = MemoCreateForm

    def get_success_url(self):
        return reverse_lazy('memo:memo_detail',kwargs={'pk':self.kwargs['pk']})

    def form_valid(self, form):
        messages.success(self.request,'メモを更新しました')
        return super().form_valid(form)

    def form_vallid(self, form):
        messages.success(self.request,'メモを更新を失敗しました')
        return super().form_valid(form)

class MemoDeleteView(LoginRequiredMixin, generic.DeleteView):
    model =  Memo
    template_name = 'memo_delete.html'
    success_url = reverse_lazy('memo:memo_list')

    def delete(self, request, *args, **kwargs):
        messages.success(self.request,"メモを削除しました。")
        return super().delete(request,*args,**kwargs)